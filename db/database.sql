CREATE DATABASE academia;
USE academia;

CREATE TABLE usuarios (
id 						int(255) auto_increment not null,
nombre					varchar (100) not null,
apellidos				varchar (255),
email					varchar (255),
password				varchar (255),
rol						varchar (20),
imagen					varchar (255),
CONSTRAINT pk_usuarios PRIMARY KEY (id),
CONSTRAINT uq_email UNIQUE(email)
)ENGINE=innoDb;

INSERT INTO usuarios VALUES(NULL, 'Jorge', 'Lirio', 'sousadrums1987@gmail.com','1234', 'admin', null);

CREATE TABLE cursos (
id 						int(255) auto_increment not null,
nombre					varchar(100) not null,
nivel					varchar(100) not null,
CONSTRAINT pk_cursos PRIMARY KEY(id)
)ENGINE=innoDb;

CREATE TABLE temas(
id 						int(255) auto_increment not null,
curso_id 				int(255) not null,
titulo					varchar(255) not null,
descripcion				text,
url						varchar(500) not null,
CONSTRAINT pk_temas PRIMARY KEY(id),
CONSTRAINT fk_tema_curso FOREIGN KEY (curso_id) REFERENCES cursos(id)
)ENGINE= innoDb;

CREATE TABLE pedidos (
id 						int(255) auto_increment not null,
usuario_id				int(255) not null,
curso_id				int(255) not null,
CONSTRAINT pk_pedidos PRIMARY KEY(id),
CONSTRAINT fk_pedido_usuario FOREIGN KEY(usuario_id) REFERENCES usuarios(id),
CONSTRAINT fk_pedido_curso FOREIGN KEY(curso_id) REFERENCES cursos(id)
)ENGINE=innoDb;

CREATE TABLE articulos(
id 						int(255) auto_increment not null,
usuario_id 				int(255) not null,
titulo					varchar(255) not null,
subtitulo				varchar(255) not null,
fechahora				datetime;
contenido				text,
imagen					varchar(500) not null,
CONSTRAINT pk_articulos PRIMARY KEY(id),
CONSTRAINT fk_articulo_usuario FOREIGN KEY (usuario_id) REFERENCES usuarios(id)
)ENGINE= innoDb;
