<div id="crear" >
<?php if(isset($edit) && isset($art) && is_object($art)): ?>
	<h1>Editar articulo <?=$art->titulo?></h1>
	<?php $url_action=base_url."articulo/save&id=".$art->id; ?>
<?php else: ?>
	<h1>Crear articulo</h1>
	<?php $url_action=base_url."articulo/save"; ?>
<?php endif; ?>
	<form action="<?=$url_action?>" method="POST" enctype="multipart/form-data">
		<label for="nombre">Título</label>
		<input type="text" name="titulo" value="<?=isset($art) && is_object($art) ? $art->titulo : '';?>" required/>

		<label for="subtitulo">Subtítulo</label>
		<input type="text" name="subtitulo" value="<?=isset($art) && is_object($art) ? $art->subtitulo : '';?>"required />
		<label for="texto">Contenido</label>
		<textarea name="texto" cols="30" rows="10"><?=isset($art) && is_object($art) ? $art->texto : '';?></textarea>
		<label for="Imagen">Imagen</label>
		<input type="file" name="imagen" value="<?=isset($art) && is_object($art) ? $art->imagen : '';?>"/>
		<input type="submit" value="Guardar" />



	</form>
</div>