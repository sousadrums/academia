<div class="articulo">
	<h2><?=$art->titulo?></h2>
	<h4><?=$art->subtitulo?> || <?=$art->fechahora?></h4>
	<img src="<?=base_url?>uploads/images/<?=$art->imagen?>" alt="Imagen">
	<p><?php echo str_replace("\n", "</p><p>", $art->texto) ;?></p>
	<h4><?=$art->nombre?> <?=$art->apellidos?></h4>
	<br>
	<?php if(isset($_SESSION['admin']) || isset($_SESSION['identity'])): ?>
		<a href="<?=base_url?>articulo/index" class="button">volver</a>
	<?php else: ?>
		<a href="<?=base_url?>" class="button">volver</a>
<?php endif; ?>
</div>