<div id="cursos">
	<?php if(isset($_SESSION['admin'])): ?>	
		<h2>Artículos</h2>
		<a href="<?=base_url?>articulo/crear" class="button">
			Crear artículo
		</a>
	<?php else: ?>
		<h2>Ver artículos</h2>
	<?php endif; ?>
	<br/>
<?php if (isset($_SESSION['delete']) && $_SESSION['delete'] == 'complete'): ?>
		<strong>Artículo borrado correctamente</strong>
<?php elseif(isset($_SESSION['delete']) && $_SESSION['delete'] == 'failed'): ?>
		<strong>Borrado fallido</strong>
<?php endif; ?>
<?php Utils::deleteSession('delete'); ?>
	<table>
		<tr>
			<th>Título</th>
			<th>Autor</th>
			<th>Fecha de creación/edición</th>
			<th>Acciones</th>
		</tr>
		<?php while ($art=$articulos->fetch_object()) : ?>
			<tr>
				<td><?=$art->titulo; ?></td>
				<td><?=$art->nombre; ?> <?=$art->apellidos; ?></td>
				<td><?=$art->fechahora; ?></td>
				<td><a href="<?=base_url?>articulo/leer&id=<?=$art->id?>" class="button">Leer</a>
					<?php if (isset($_SESSION['admin'])) : ?>
						<a href="<?=base_url?>articulo/edit&id=<?=$art->id?>" class="button button-alert">Editar</a>
						<a href="<?=base_url?>articulo/delete&id=<?=$art->id?>" class="button button-danger">Eliminar</a>
					<?php endif; ?>

				</td>
			</tr>
		<?php endwhile; ?>
	</table>
</div>