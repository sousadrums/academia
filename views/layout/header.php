
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Academia</title>
	<link rel="stylesheet" href="<?=base_url?>assets/css/styles.css" />
</head>
<body>
	<div id="container">
		<!--Cabecera-->
		<header id="header">
			<div id="logo">
				<img src="<?=base_url?>assets/img/gogo.jpg" alt="logo"/>
				<a href="<?=base_url?>"><h1 id="logo">Jorge Lirio</h1></a>
			</div>
		</header>
		<!--Menu-->
		<div id="menus">		
			<nav id="menu1">
				<ul>
					<li><a href="<?=base_url?>">Inicio</a></li>
					<li><a href="<?=base_url?>curso/index">Cursos</a></li>
					<li><a href="<?=base_url?>usuario/cv">Sobre mí</a></li>
				</ul>
			</nav>
			<?php if(!isset($_SESSION['identity'])): ?>
				<nav id="menu2">
					<ul>
						<li><a href="<?=base_url?>usuario/loginV">Inicio sesión</a></li>
						<li><a href="<?=base_url?>usuario/register">regístrate</a></li>
					</ul>
				</nav>
			<?php else: ?>
				<nav id="menu2">
					<ul>
						<li><a href="<?=base_url?>usuario/loged"><?=$_SESSION['identity']->nombre?> <?=$_SESSION['identity']->apellidos?> </a></li>
						<li><a href="<?=base_url?>usuario/logout">Cerrar sesión</a></li>
					</ul>
				</nav>
			<?php endif; ?>
		</div><br>