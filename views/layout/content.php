<!--Contenido-->
		<div id="inicio">
			<h2>Últimos artículos</h2>
			<?php while ($art=$articulos->fetch_object()) : ?>
				<div class="articulo" id="min">
					<h2><?=$art->titulo?></h2>
					<h4><?=$art->nombre?> <?=$art->apellidos?> || <?=$art->fechahora?></h4>
					<img id="miniatura" src="<?=base_url?>uploads/images/<?=$art->imagen?>" alt="imagen">
					<p><?= substr($art->texto, 0, 200)."..." ;?></p>
					<br>
					<a href="<?=base_url?>articulo/leer&id=<?=$art->id?>" class="button">leer mas</a>
				</div>
			<?php endwhile; ?>
		</div>