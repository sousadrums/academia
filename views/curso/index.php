<div id="cursos">
	<?php if(isset($_SESSION['admin'])): ?>	
		<h2>Cursos</h2>
		<a href="<?=base_url?>curso/crear" class="button">
			Crear curso
		</a>
	<?php elseif(isset($_SESSION['identity'])): ?>
		<h2>Cursos</h2>
	<?php else: ?>
		<h2>Cursos disponibles</h2>
	<?php endif; ?>
	<br/>
<?php if (isset($_SESSION['delete']) && $_SESSION['delete'] == 'complete'): ?>
		<strong>Curso borrado correctamente</strong>
<?php elseif(isset($_SESSION['delete']) && $_SESSION['delete'] == 'failed'): ?>
		<strong>Borrado fallido. Deberás borrar primero los temas del curso</strong>
<?php endif; ?>
<?php Utils::deleteSession('delete'); ?>
<br>
	<table>
		<tr>
			<th>Nombre</th>
			<th>Nivel</th>
			<th>Acciones</th>
		</tr>
		<?php while ($cur=$cursos->fetch_object()) : ?>
			<tr>
				<td><?=$cur->nombre; ?></td>
				<td><?=$cur->nivel; ?></td>
				<td>
				<?php if(isset($_SESSION['admin'])) : ?>
					<a href="<?=base_url?>tema/index&id=<?=$cur->id?>" class="button">Acceder</a>
					<a href="<?=base_url?>curso/edit&id=<?=$cur->id?>" class="button button-alert">Editar</a>
					<a href="<?=base_url?>curso/delete&id=<?=$cur->id?>" class="button button-danger">Eliminar</a>
				<?php elseif(isset($_SESSION['identity'])) : ?>
					<a href="<?=base_url?>tema/index&id=<?=$cur->id?>" class="button">Acceder</a>
				<?php else : ?>
					<a href="<?=base_url?>tema/index&id=<?=$cur->id?>" class="button">Vista Previa</a>
				<?php endif; ?>

				</td>
			</tr>
		<?php endwhile; ?>
	</table>
</div>