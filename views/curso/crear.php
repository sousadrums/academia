<div id="crear" >
<?php if(isset($edit) && isset($cur) && is_object($cur)): ?>
	<h1>Editar curso <?=$cur->nombre?> <?=$cur->nivel?></h1>
	<?php $url_action=base_url."curso/save&id=".$cur->id; ?>
<?php else: ?>
	<h1>Crear curso</h1>
	<?php $url_action=base_url."curso/save"; ?>
<?php endif; ?>
	<form action="<?=$url_action?>" method="POST">
		<label for="nombre">Nombre</label>
		<input type="text" name="nombre" value="<?=isset($cur) && is_object($cur) ? $cur->nombre : '';?>" required/>

		<label for="nivel">Nivel</label>
		<input type="text" name="nivel" value="<?=isset($cur) && is_object($cur) ? $cur->nivel : '';?>"required />
		<input type="submit" value="Guardar" />
	</form>
<a class="button" id="atras" href="#">Atrás</a>
</div>