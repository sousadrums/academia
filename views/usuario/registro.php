<div id="registro" class="formulario">
	<?php if (isset($_SESSION['register']) && $_SESSION['register'] == 'complete'): ?>
		<strong>Registro completado correctamente</strong>
<?php elseif(isset($_SESSION['register']) && $_SESSION['register'] == 'failed'): ?>
		<strong>Registro fallido</strong>
<?php endif; ?>
<?php Utils::deleteSession('register'); ?>

	<h1>Registrarse</h1>
	<form action="<?=base_url?>usuario/save" method="POST">
		<label for="nombre">Nombre</label>
		<input type="text" name="nombre" required /><br>
		<label for="apellidos">Apellidos</label>
		<input type="text" name="apellidos" required /><br>
		<label for="email">email</label>
		<input type="email" name="email" required /><br>
		<label for="password">Contraseña</label>
		<input type="password" name="password" required /><br>
		<input type="submit" value="Registrarse" />
	</form>
</div>