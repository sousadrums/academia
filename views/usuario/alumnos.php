<div id="cursos">
		<h2>Alumnos</h2>
	<br/>
	<table>
		<tr>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Acciones</th>
		</tr>
		<?php while ($alu=$usuarios->fetch_object()) : ?>
			<tr>
				<td><?=$alu->nombre; ?></td>
				<td><?=$alu->apellidos; ?></td>
				<td><a href="<?=base_url?>usuario/alumnos&id=<?=$alu->id?>" class="button">cursos</a>
					<?php if (isset($_SESSION['admin'])) : ?>
						<a href="<?=base_url?>usuario/delete&id=<?=$alu->id?>" class="button button-danger">Eliminar</a>
					<?php endif; ?>

				</td>
			</tr>
		<?php endwhile; ?>
	</table>
</div>