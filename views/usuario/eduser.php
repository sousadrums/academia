<div  class="formulario">
	<h1>Editar datos de usuario</h1>
	<form action="<?=base_url?>usuario/update" method="POST">
		<label for="nombre">Nombre</label>
		<input type="text" name="nombre" value="<?=isset($nombre)  ? $nombre : '';?>" required /><br>
		<label for="apellidos" >Apellidos</label>
		<input type="text" name="apellidos" value="<?=isset($apellidos)  ? $apellidos : '';?>"required /><br>
		<label for="email">email</label>
		<input type="email" name="email" value="<?=isset($email)  ? $email : '';?>"required /><br>
		<input type="submit" value="Guardar" />
	</form>
	<br>
	<a href="<?=base_url?>usuario/contraseña" class="button">Cambiar contraseña</a>
</div>