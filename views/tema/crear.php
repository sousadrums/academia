<div id="crear" >
<?php if(isset($edit) && isset($tem) && is_object($tem)): ?>
	<h1>Editar tema <?=$tem->titulo?></h1>
	<?php $url_action=base_url."tema/editar&id=".$tem->id; ?>
<?php else: ?>
	<h1>Crear tema</h1>
	<?php $url_action=base_url."tema/save&id=".$_GET['id']; ?>
<?php endif; ?>
	<form action="<?=$url_action?>" method="POST" enctype="multipart/form-data">
		<label for="titulo">Título</label>
		<input type="text" name="titulo" value="<?=isset($tem) && is_object($tem) ? $tem->titulo : '';?>" required/>
		<label for="descripcion">Descripción</label>
		<textarea name="descripcion" cols="30" rows="10"><?=isset($tem) && is_object($tem) ? $tem->descripcion : '';?></textarea>
		<label for="url">Video</label>
		<input type="file" name="url" required />

		<input type="submit" value="Guardar" />

	</form>
</div>