<div id="cursos">
	<?php if(isset($_SESSION['admin'])): ?>	
		<h2>Temas</h2>
		<a href="<?=base_url?>tema/crear&id=<?=$_GET['id']?>" class="button">
			Crear tema
		</a>
	<?php elseif(isset($_SESSION['identity'])): ?>
		<h2>temas</h2>
	<?php else: ?>
		<h2>Temas disponibles</h2>
	<?php endif; ?>
	<a href="<?=base_url?>curso/index" class="button" id="atras">Atras</a>
	<br/>
<?php if (isset($_SESSION['delete']) && $_SESSION['delete'] == 'complete'): ?>
		<strong>Tema borrado correctamente</strong>
<?php elseif(isset($_SESSION['delete']) && $_SESSION['delete'] == 'failed'): ?>
		<strong>Borrado fallido</strong>
<?php endif; ?>
<?php Utils::deleteSession('delete'); ?>
	<table>
		<tr>
			<th>Titulo</th>
			<th>Descripción</th>
			<th>Acciones</th>
		</tr>
		<?php while ($tem=$temas->fetch_object()) : ?>
			<tr>
				<td><?=$tem->titulo; ?></td>
				<td><?=$tem->descripcion; ?></td>
				<td>
				<?php if(isset($_SESSION['admin'])) : ?>
					<a href="<?=base_url?>tema/tema&id=<?=$tem->id?>" class="button">Acceder</a>
					<a href="<?=base_url?>tema/edit&id=<?=$tem->id?>" class="button button-alert">Editar</a>
					<a href="<?=base_url?>tema/delete&id=<?=$tem->id?>" class="button button-danger">Eliminar</a>
				<?php elseif(isset($_SESSION['identity'])) : ?>
					<a href="<?=base_url?>tema/tema&id=<?=$tem->id?>" class="button">Acceder</a>
				<?php else : ?>
					<a href="<?=base_url?>tema/previa&id=<?=$tem->id?>" class="button">Vista Previa</a>
				<?php endif; ?>

				</td>
			</tr>
		<?php endwhile; ?>
	</table>
</div>