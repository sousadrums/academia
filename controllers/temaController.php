<?php
require_once 'models/tema.php';
class temaController{


	public function index(){
		if (isset($_GET['id'])) {
			$curso_id=$_GET['id'];
			$tema=new Tema();
			$tema->setCursoId($curso_id);
			$temas=$tema->getTemasCurso();
			if (isset($_SESSION['admin'])) {
				require_once 'views/usuario/admin.php';	
			}elseif(isset($_SESSION['identity'])){
				require_once 'views/usuario/user.php';
			}else{
			}
		require_once 'views/tema/index.php';
		}
	}
	public function tema(){
		if (isset($_GET['id'])) {
			$id=$_GET['id'];

			$tema=new Tema();
			$tema->setId($id);
			$curid=$tema->recuperarCursoId();
			$tema->setCursoId($curid);
			$tem=$tema->getOne();
			$temas=$tema->getTemasCurso();
			$nombre=$tema->getNombreCurso();
		}
		if (isset($_SESSION['admin'])) {
			require_once 'views/usuario/admin.php';	
		}elseif(isset($_SESSION['identity'])){
			require_once 'views/usuario/user.php';
		}else{
		}
		require_once 'views/tema/tema.php';
		require_once 'views/tema/lista.php';
	}

	public function previa(){
		if (isset($_GET['id'])) {
			$id=$_GET['id'];

			$tema=new Tema();
			$tema->setId($id);
			$curid=$tema->recuperarCursoId();
			$tema->setCursoId($curid);
			$tem=$tema->getOne();
			$temas=$tema->getTemasCurso();
			$nombre=$tema->getNombreCurso();
		}
		require_once 'views/tema/tema.php';
		require_once 'views/tema/lista.php';
	}
	
	public function crear(){
		Utils::isAdmin();
		require_once 'views/usuario/admin.php';
		require_once 'views/tema/crear.php';
	}

	public function save(){
		Utils::isAdmin();

		if (isset($_POST)) {
			$tema= new Tema();
			$tema->setCursoId($_GET['id']);
			$tema->setTitulo($_POST['titulo']);
			$tema->setDescripcion($_POST['descripcion']);
			

			//guardar video
			$file=$_FILES['url'];
			$filename=$file['name'];
			$mimetype=$file['type'];

			

			if ($mimetype=="video/mp4") {
				if (!is_dir('uploads/video')) {
					mkdir('uploads/video', 0777, true);
				}
				move_uploaded_file($file['tmp_name'], 'uploads/video/'.$filename);
				$tema->setUrl($filename);
			}

			$save=$tema->save();
			
			if ($save) {
				$_SESSION['tema']="complete";
			}else{
				$_SESSION['tema']="failed";
			}
		}else{
			$_SESSION['tema']="failed";
		}
		header("Location:".base_url."tema/index&id=".$tema->getCursoId());
	}

	
	public function edit(){
		Utils::isAdmin();
		if (isset($_GET['id'])) {
			$id=$_GET['id'];
			$edit=true;

			$tema=new Tema();
			$tema->setId($id);
			$tem=$tema->getOne();


			require_once 'views/usuario/admin.php';
			require_once 'views/tema/crear.php';
		}else{
			header("Location:".base_url."tema/index");
		}

	}

	public function editar(){
		Utils::isAdmin();

		if (isset($_POST)) {
			$tema= new Tema();
			$tema->setId($_GET['id']);
			$tema->setTitulo($_POST['titulo']);
			$tema->setDescripcion($_POST['descripcion']);
			

			//guardar video
			$file=$_FILES['url'];
			$filename=$file['name'];
			$mimetype=$file['type'];

			

			if ($mimetype=="video/mp4") {
				if (!is_dir('uploads/video')) {
					mkdir('uploads/video', 0777, true);
				}
				move_uploaded_file($file['tmp_name'], 'uploads/video/'.$filename);
				$tema->setUrl($filename);
			}

			$save=$tema->edit();
			$curid=$tema->recuperarCursoId();
			
			
			if ($save) {
				$_SESSION['tema']="complete";
			}else{
				$_SESSION['tema']="failed";
			}
		}else{
			$_SESSION['tema']="failed";
		}
		header("Location:".base_url."tema/index&id=".$curid);

	}

	public function delete(){
		Utils::isAdmin();

		if (isset($_GET['id'])) {
			$id=$_GET['id'];
			$tema= new Tema();
			$tema->setId($id);
			$cursoid=$tema->recuperarCursoId();
			$delete=$tema->delete();
			if ($delete) {
				$_SESSION['delete']='complete';
			}else{
				$_SESSION['delete']='failed';
			}

		}else{
			$_SESSION['delete']='failed';
		}

		header("Location:".base_url."tema/index&id=".$cursoid);
	}

}
?>