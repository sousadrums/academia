<?php

require_once 'models/curso.php';

class cursoController{

	public function index(){	
		$curso=new Curso();
		$cursos=$curso->getAll();
		if (isset($_SESSION['admin'])) {
			require_once 'views/usuario/admin.php';	
		}elseif(isset($_SESSION['identity'])){
			require_once 'views/usuario/user.php';
		}else{
		}
		require_once 'views/curso/index.php';
	}
	

	public function crear(){
		Utils::isAdmin();
		require_once 'views/usuario/admin.php';
		require_once 'views/curso/crear.php';
	}

	public function save(){
		Utils::isAdmin();

		if (isset($_POST)) {
			$curso= new Curso();
			$curso->setNombre($_POST['nombre']);
			$curso->setNivel($_POST['nivel']);

			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$curso->setId($id);
				$save=$curso->edit();
			}else{
				$save=$curso->save();
			}

			if ($save) {
				$_SESSION['curso']="complete";
			}else{
				$_SESSION['curso']="failed";
			}
		}else{
			$_SESSION['curso']="failed";
		}

		header("Location:".base_url."/curso/index");
	}
	public function temas(){
		Utils::isAdmin();

	}
	public function edit(){
		Utils::isAdmin();
		if (isset($_GET['id'])) {
			$id=$_GET['id'];
			$edit=true;

			$curso=new Curso();
			$curso->setId($id);
			$cur=$curso->getOne();

			require_once 'views/usuario/admin.php';
			require_once 'views/curso/crear.php';
		}else{
			header("Location:".base_url."/curso/index");
		}

	}
	public function delete(){
		Utils::isAdmin();
			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$curso= new Curso();
				$curso->setId($id);
				$delete=$curso->delete();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."curso/index");
	}





}




?>