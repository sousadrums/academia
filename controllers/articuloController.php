<?php

require_once 'models/articulo.php';

class articuloController{

	public function destacados(){
		$articulo=new Articulo();
		$articulos=$articulo->getAllAutor();
		require_once 'views/layout/content.php';

	}

public function index(){
		$articulo=new Articulo();
		$articulos=$articulo->getAllAutor();
		if (isset($_SESSION['admin'])) {
			require_once 'views/usuario/admin.php';	
		}else{
			require_once 'views/usuario/user.php';
		}
		
		require_once 'views/articulo/index.php';

	}

	public function crear(){
		Utils::isAdmin();
		require_once 'views/usuario/admin.php';
		require_once 'views/articulo/crear.php';
	}

	public function save(){
		Utils::isAdmin();

		if (isset($_POST)) {
			$articulo= new Articulo();
			$articulo->setUsuarioId($_SESSION['identity']->id);
			$articulo->setTitulo($_POST['titulo']);
			$articulo->setSubtitulo($_POST['subtitulo']);
			$articulo->setTexto($_POST['texto']);
			

			//guardar imagen
			$file=$_FILES['imagen'];
			$filename=$file['name'];
			$mimetype=$file['type'];

			

			if ($mimetype=="image/jpeg" || $mimetype=="image/jpg" || $mimetype== "image/jpg" || $mimetype== "image/gif") {
				if (!is_dir('uploads/images')) {
					mkdir('uploads/images', 0777, true);
				}
				move_uploaded_file($file['tmp_name'], 'uploads/images/'.$filename);
				$articulo->setImagen($filename);
			}




				
			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$articulo->setId($id);
				$save=$articulo->edit();
			}else{
				$save=$articulo->save();
			}

			if ($save) {
				$_SESSION['articulo']="complete";
			}else{
				$_SESSION['articulo']="failed";
			}
		}else{
			$_SESSION['articulo']="failed";
		}

		header("Location:".base_url."/articulo/index");
	}

	public function leer(){
		if (isset($_GET['id'])) {
			$id=$_GET['id'];
		
			$articulo=new Articulo();
			$articulo->setId($id);
			$art=$articulo->getOneAutor();

			if (isset($_SESSION['admin'])) {
				require_once 'views/usuario/admin.php';	
			}elseif(isset($_SESSION['identity'])){
				require_once 'views/usuario/user.php';
			}else{

			}
			require_once 'views/articulo/leer.php';
		}
	}

	public function edit(){
		Utils::isAdmin();
		if (isset($_GET['id'])) {
			$id=$_GET['id'];
			$edit=true;

			$articulo=new Articulo();
			$articulo->setId($id);
			$art=$articulo->getOne();

			require_once 'views/usuario/admin.php';
			require_once 'views/articulo/crear.php';
		}else{
			header("Location:".base_url."articulo/index");
		}

	}

	public function delete(){
		Utils::isAdmin();

		if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$articulo= new Articulo();
				$articulo->setId($id);
			
				$delete=$articulo->delete();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}
			header("Location:".base_url."/articulo/index");


	}


}

?>