<?php 

class Articulo{
	private $id;
	private $usuario_id;
	private $titulo;
	private $subtitulo;
	private $fechahora;
	private $texto;
	private $imagen;
	private $autor;

	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}

	function getId(){
		return $this->id;
	}
	function getUsuarioId(){
		return $this->usuario_id;
	}
	function getTitulo(){
		return $this->titulo;
	}
	function getSubtitulo(){
		return $this->subtitulo;
	}
	function getFechahora(){
		return $this->fechahora;
	}
	function getTexto(){
		return $this->texto;
	}
	
	function getImagen(){
		return $this->imagen;
	}
	function setId($id){
		$this->id=$id;
	}
	function setUsuarioId($usuario_id){
		$this->usuario_id=$usuario_id;
	}
	function setTitulo($titulo){
		$this->titulo=$this->db->real_escape_string($titulo);
	}
	function setSubtitulo($subtitulo){
		$this->subtitulo=$this->db->real_escape_string($subtitulo);
	}
	function setFechahora($fechahora){
		$this->fechahora=$this->db->real_escape_string($fechahora);
	}
	function setTexto($texto){
		$this->texto=$this->db->real_escape_string($texto);
	}
	function setImagen($imagen){
		$this->imagen=$this->db->real_escape_string($imagen);
	}

	public function getAll(){
		$articulos= $this->db->query("SELECT * FROM articulos ORDER BY id DESC;");
		return $articulos;
	}
	public function getAllAutor(){
		$articulos= $this->db->query("SELECT a.*, u.*, a.imagen, a.id FROM articulos a INNER JOIN usuarios u ON a.usuario_id = u.id ORDER BY a.fechahora DESC;");
		return $articulos;
	}

	public function save(){
		$sql = "INSERT INTO articulos VALUES(NULL, {$_SESSION['identity']->id}, '{$this->getTitulo()}', '{$this->getSubtitulo()}', NOW(), '{$this->getTexto()}', '{$this->getImagen()}')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	public function delete(){
		$sql="DELETE FROM articulos WHERE id={$this->id}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}
	public function getOne(){
		$articulo= $this->db->query("SELECT * FROM articulos WHERE id={$this->getId()}");
		return $articulo->fetch_object();
	}
	public function getOneAutor(){
		$articulo= $this->db->query("SELECT a.*, u.*, a.imagen, a.id FROM articulos a INNER JOIN usuarios u ON a.usuario_id = u.id WHERE a.id={$this->getid()}");

		return $articulo->fetch_object();
	}

	public function edit(){
		$sql = "UPDATE articulos SET titulo='{$this->getTitulo()}', subtitulo='{$this->getSubtitulo()}', fechahora=NOW(), texto='{$this->getTexto()}', imagen='{$this->getImagen()}' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}


}


 ?>