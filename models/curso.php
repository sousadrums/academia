<?php

class Curso{
	private $id;
	private $nombre;
	private $nivel;

	public function __construct(){
		$this->db = Database::connect();

	}

	function getId(){
		return $this->id;
	}
	function getNombre(){
		return $this->nombre;
	}
	function getNivel(){
		return $this->nivel;
	}
	function setId($id){
		$this->id=$id;
	}
	function setNombre($nombre){
		$this->nombre=$this->db->real_escape_string($nombre);
	}
	function setNivel($nivel){
		$this->nivel=$this->db->real_escape_string($nivel);
	}

	public function getAll(){
		$cursos= $this->db->query("SELECT * FROM cursos ORDER BY id DESC;");
		return $cursos;
	}

	public function getOne(){
		$curso= $this->db->query("SELECT * FROM cursos WHERE id={$this->getId()};");
		return $curso->fetch_object();
	}

	public function save(){
		$sql = "INSERT INTO cursos VALUES(NULL, '{$this->getNombre()}', '{$this->getNivel()}')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function edit(){
		$sql = "UPDATE cursos SET nombre='{$this->getNombre()}', nivel='{$this->getNivel()}' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function delete(){
		$sql="DELETE FROM cursos WHERE id={$this->id}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

}


?>