<?php 

class Tema{
	private $id;
	private $curso_id;
	private $titulo;
	private $descripcion;
	private $url;
	private $db;

	public function __construct(){
		$this->db = Database::connect();

		}

	function getId(){
		return $this->id;
	}
	function getCursoId(){
		return $this->curso_id;
	}
	function getTitulo(){
		return $this->titulo;
	}
	function getDescripcion(){
		return $this->descripcion;
	}
	function getUrl(){
		return $this->url;
	}

	function setId($id){
		$this->id=$id;
	}
	function setCursoId($curso_id){
		$this->curso_id=$this->db->real_escape_string($curso_id);
	}
	function setTitulo($titulo){
		$this->titulo=$this->db->real_escape_string($titulo);
	}
	function setDescripcion($descripcion){
		$this->descripcion=$this->db->real_escape_string($descripcion);
	}
	function setUrl($url){
		$this->url=$this->db->real_escape_string($url);
	}
	function getTemasCurso(){
		$sql="SELECT * FROM temas WHERE curso_id={$this->getCursoId()} ORDER BY id DESC";
		$temas= $this->db->query($sql);
		return $temas;
	}
	public function getOne(){
		$tema= $this->db->query("SELECT * FROM temas WHERE id={$this->getId()};");
		return $tema->fetch_object();
	}

	public function save(){
		$sql = "INSERT INTO temas VALUES(NULL, {$this->getCursoId()}, '{$this->getTitulo()}', '{$this->getDescripcion()}', '{$this->getUrl()}')";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function recuperarCursoId(){
		$cursoid= $this->db->query("SELECT curso_id FROM temas WHERE id={$this->getId()};");
		$curarray=mysqli_fetch_assoc($cursoid);
		$curid=$curarray['curso_id'];
		return $curid;
	}

	public function getNombreCurso(){
		$tema= $this->db->query("SELECT c.nombre, c.nivel FROM temas t INNER JOIN cursos c ON t.curso_id = c.id WHERE t.id={$this->getid()}");
		return $tema->fetch_object();
	}

	public function edit(){
		$sql = "UPDATE temas SET titulo='{$this->getTitulo()}', descripcion='{$this->getDescripcion()}', url='{$this->getUrl()}' WHERE id={$this->id}";
		$save= $this->db->query($sql);
		
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}

	public function delete(){
		$sql="DELETE FROM temas WHERE id={$this->id}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}



}


 ?>