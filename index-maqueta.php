<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Academia</title>
	<link rel="stylesheet" href="assets/css/styles.css" />
</head>
<body>
	<div id="container">
		<!--Cabecera-->
		<header id="header">
			<div id="logo">
				<img src="assets/img/Logo.jpg" alt="logo"/>
				<a href="index.php"><h1>ACADEMIA</h1></a>
			</div>
		</header>
		<!--Menu-->
		<div id="menus">		
			<nav id="menu1">
				<ul>
					<li><a href="#">Inicio</a></li>
					<li><a href="#">Cursos</a></li>
					<li><a href="#">Conócenos</a></li>
				</ul>
			</nav>
			<nav id="menu2">
				<ul>
					<li><a href="#">Inicio sesión</a></li>
					<li><a href="#">regístrate</a></li>
				</ul>
			</nav>
		</div>
		<!--Contenido-->
		<div id="inicio">
			<div class="articulo">
				<h2>titulo</h2>
				<h4>Descripción</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, odit veniam! Ratione repellat eum, delectus, excepturi officia exercitationem labore rem itaque in voluptatibus animi nostrum consequuntur a sit modi.</p>
				<p>Quisquam facilis qui commodi tempore atque quam non reiciendis nobis doloribus voluptatem, iusto necessitatibus aspernatur dolorum. Minima ducimus cumque, voluptates, adipisci natus, saepe magni dolorum ullam fugit, cum itaque obcaecati.</p>
				<p>Quasi adipisci assumenda totam sint reprehenderit? Mollitia voluptatum culpa voluptates sequi consequatur voluptate placeat ipsa beatae ducimus laboriosam accusantium pariatur magnam expedita cumque, fuga velit quod ex reprehenderit odio sed.</p>
				<p>Molestiae eligendi cum laudantium quae, natus nisi sapiente vitae pariatur culpa, debitis ducimus facilis commodi perferendis soluta qui iure, molestias sit eveniet ullam fugiat sed id non incidunt. Sapiente, similique.</p>
				<p>Debitis eaque, enim harum nostrum obcaecati repellendus commodi non aperiam eligendi earum ea omnis temporibus et cumque atque libero soluta saepe, ratione ab magnam incidunt unde? Maiores excepturi, quasi suscipit.</p>
			</div>
			<div class="articulo">
				<h2>titulo</h2>
				<h4>Descripción</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, odit veniam! Ratione repellat eum, delectus, excepturi officia exercitationem labore rem itaque in voluptatibus animi nostrum consequuntur a sit modi.</p>
				<p>Quisquam facilis qui commodi tempore atque quam non reiciendis nobis doloribus voluptatem, iusto necessitatibus aspernatur dolorum. Minima ducimus cumque, voluptates, adipisci natus, saepe magni dolorum ullam fugit, cum itaque obcaecati.</p>
				<p>Quasi adipisci assumenda totam sint reprehenderit? Mollitia voluptatum culpa voluptates sequi consequatur voluptate placeat ipsa beatae ducimus laboriosam accusantium pariatur magnam expedita cumque, fuga velit quod ex reprehenderit odio sed.</p>
				<p>Molestiae eligendi cum laudantium quae, natus nisi sapiente vitae pariatur culpa, debitis ducimus facilis commodi perferendis soluta qui iure, molestias sit eveniet ullam fugiat sed id non incidunt. Sapiente, similique.</p>
				<p>Debitis eaque, enim harum nostrum obcaecati repellendus commodi non aperiam eligendi earum ea omnis temporibus et cumque atque libero soluta saepe, ratione ab magnam incidunt unde? Maiores excepturi, quasi suscipit.</p>
			</div>
		</div>

		<!--Footer-->
		<footer id="footer">
			<p>
				Desarrollado por Jorge &copy <?=Date('yy') ?>
			</p>
		</footer>
	</div>
	<!---->
</body>
</html>